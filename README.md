<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# core 0.3.69

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_core/develop?logo=python)](
    https://gitlab.com/ae-group/ae_core)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_core/release0.3.68?logo=python)](
    https://gitlab.com/ae-group/ae_core/-/tree/release0.3.68)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_core)](
    https://pypi.org/project/ae-core/#history)

>ae_core module 0.3.69.

[![Coverage](https://ae-group.gitlab.io/ae_core/coverage.svg)](
    https://ae-group.gitlab.io/ae_core/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_core/mypy.svg)](
    https://ae-group.gitlab.io/ae_core/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_core/pylint.svg)](
    https://ae-group.gitlab.io/ae_core/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_core)](
    https://gitlab.com/ae-group/ae_core/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_core)](
    https://gitlab.com/ae-group/ae_core/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_core)](
    https://gitlab.com/ae-group/ae_core/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_core)](
    https://pypi.org/project/ae-core/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_core)](
    https://gitlab.com/ae-group/ae_core/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_core)](
    https://libraries.io/pypi/ae-core)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_core)](
    https://pypi.org/project/ae-core/#files)


## installation


execute the following command to install the
ae.core module
in the currently active virtual environment:
 
```shell script
pip install ae-core
```

if you want to contribute to this portion then first fork
[the ae_core repository at GitLab](
https://gitlab.com/ae-group/ae_core "ae.core code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_core):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_core/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.core.html
"ae_core documentation").
